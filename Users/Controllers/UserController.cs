﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Users.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly DataContext _context;
        public UserController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<User>>> Get()
        {
            return Ok(await _context.Users.ToListAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
                return BadRequest("User Not Found");
            return Ok(user);
        }

        [HttpPost]
        public async Task<ActionResult<List<User>>> StoreUser(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return Ok(user);
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult<List<User>>> UpdateUser(User request, int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
                return BadRequest("User Not Found");

            user.firstName = request.firstName;
            user.lastName = request.lastName;
            user.email = request.email;
            user.password = request.password;

            await _context.SaveChangesAsync();

            return Ok(user);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<List<User>>> DeleteUser(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
                return BadRequest("User Not Found");

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return Ok("success");
        }
    }
}